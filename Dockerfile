FROM node:15.4.0-alpine3.10

RUN mkdir -p /usr/local/app

WORKDIR /usr/local/app

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 8080

CMD ["npm", "start"]