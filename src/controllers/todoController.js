import todoList from '../mock/todoList.js';

class TodoController {


    static getTodoList(req, res) {
          return res.status(200).json({
                todoList,
                message: "All the TodoList",
          });
    }

    static getTodoItem(req, res) {
           const findTodo = todoList.find(todoItem => todoItem.id === parseInt(req.params.id, 10));
           if (findTodo) {
               return res.status(200).json({
                    todoItem: findTodo,
                     message: "Todo Item Found",
               });
           }
           return res.status(404).json({
                 message: "Todo Item Not Found",
           });
    }
}export default TodoController;