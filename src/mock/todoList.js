const todoList = [
    {
        id: 1,
        title: 'Task 1',
        description: "",
        completed: false
    },
    {
        id: 2,
        title: 'Task 2',
        description: "",
        completed: false
    },
    {
        id: 3,
        title: 'Task 3',
        description: "",
        completed: false
    },
];

export default todoList;