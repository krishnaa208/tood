import express from 'express';
import TodoController from './controllers/todoController.js';


const { Router } = express;
const app = express();
const port = process.env.PORT || 8080;



const routes = Router();
routes.get('/', TodoController.getTodoList);
routes.get('/:id', TodoController.getTodoItem);

app.use('/', routes);// Start our server

app.listen(port, () => {
    console.log(`Server started on port ${port}`);
});

export default app;
