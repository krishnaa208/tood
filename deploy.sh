#!/usr/bin/env bash

# Variables


# Local Command
createRepo () {
    # aws ecr create-repository --repository-name ezest/todo-app
    aws ecr create-repository --repository-name $IMAGE_NAME
    echo Created ECR repository: $IMAGE_NAME.
}

buildImage () {
    # docker build -f Dockerfile . -t ezest/todo-app:latest
    echo Building Image...
    docker build -f Dockerfile . -t $IMAGE_NAME:latest
    echo Build complete.
}

# Runs the container locally.
runContainer () {
    # docker run --rm --name todo-app-container -p 8080:8080 -d ezest/todo-app
    # netstat -ano | findStr "8080"
    # Stop-Process -ID PID -Force
    docker run --rm \
        --name $CONTAINER_NAME \
        -p $HOST_PORT:$CONTAINER_PORT \
        -e "Environment=development" \
        -d $IMAGE_NAME
    echo Container started. Open browser at http://localhost:$HOST_PORT .
}

pushImage () {
    # docker tag ezest/todo-app:latest <yourAWSaccountnumber>.dkr.ecr.us-east-2.amazonaws.com/ezest/todo-app:latest
    # docker push <yourAWSaccountnumber>.dkr.ecr.us-east-2.amazonaws.com/ezest/todo-app:latest
    docker tag $IMAGE_NAME:latest $FULLY_QUALIFIED_IMAGE_NAME:latest
    # eval "$(aws ecr get-login --no-include-email --region $AWS_REGION)"
    docker push $FULLY_QUALIFIED_IMAGE_NAME:latest
}


if [ $# -eq 0 ]; then
  echo "No Argument Provided"
else
  case "$1" in
      "build")
             buildImage
             ;;
      "run")
             runContainer
             ;;
      "buildpush")
             buildImage
             pushImage
             ;;
      "push")
             pushImage
             ;;
      "createrepo")
             createRepo
             ;;
      "buildrun")
             buildImage
             runContainer
             ;;
      *)
             showUsage
             ;;
  esac
fi


aws cloudformation deploy --stack-name ecs-stack --template ./cloudformation/ecs-stack.yml --capabilities CAPABILITY_IAM --parameter-overrides KeyName="key-03c8d238b76cdee47" VpcId="vpc-a7bc74cc" SubnetId="subnet-a01937da,subnet-ee0395a2" ContainerPort="8080" DesiredCapacity="1" EcsImageUri="269717866932.dkr.ecr.us-east-2.amazonaws.com/ezest/todo-app" EcsImageVersion="latest" InstanceType="t2.micro" MaxSize="2"